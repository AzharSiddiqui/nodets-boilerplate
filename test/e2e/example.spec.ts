import 'reflect-metadata';
import { BasicController } from '../../src/server/controllers/v1/basic';
import { cleanUpMetadata } from 'inversify-express-utils';
import { Container } from 'inversify';
import { GlobalSetup } from './global.setup';
import { BasicService } from '../../src/services/basic';
import { types } from '../../src/types';




describe('#Test basic routes files', () => {
    let container: Container;

    beforeAll(async () => {


        container = await new GlobalSetup().init();

        // binding controller to be accessed later inside test block
        container.bind<BasicController>('basiccontroller').to(BasicController);
    })

    beforeEach(() => {
        cleanUpMetadata();
    });

    describe("#GET health", () => {


        it("test the response of health api", async () => {
            let controller: BasicController = container.get('basiccontroller');
            const { data } = await controller.healthCheck();
            expect(data).toContainKeys(['health', 'color']);
            expect(data).toContainEntries([['health', 'OK'], ['color', 'Green']]);
        });

        it('test with mock function call', async () => {

            let controller: BasicController = container.get('basiccontroller');
            const service = container.get<BasicService>(types.BasicService);
            const healthmock = jest.spyOn(service, 'health');

            // override implementation of original function
            healthmock.mockImplementation(()=> {
                return {
                    success: true,
                    data: {
                        health: 'OK',
                        color: 'Blue'
                    }
                }
            });

            const { data } = await controller.healthCheck();
            expect(data).toContainKeys(['health', 'color']);
            expect(data).toContainEntries([['health', 'OK'], ['color', 'Blue']]);
        });

        it('test for checkORM()',async () => {
            let controller: BasicController = container.get('basiccontroller');
            const result = await controller.checkORM();
            expect(result).toContainEntry(['success', true]);
        });
    });
})
