import 'reflect-metadata';
import { Container } from "inversify";
import { container } from "../../src/container.config";
import { B2CServer } from "../../src/server/server";
import { types } from "../../src/types";
import { Logger } from "../../src/config/logger";
import { PG } from "../../src/db/pg";
import { ErrorsHandler } from "../../src/server/middlewares/errorHandler";
import { ORMManager } from "../../src/db/db";
import { Redis } from "../../src/db/redis";
import { BasicService } from "../../src/services/basic";
import { createConnection } from "typeorm";

import * as dotenv from 'dotenv';

dotenv.config();

export class GlobalSetup {

    public container: Container;
    public async init() : Promise<Container> {
        await createConnection({
            type: "postgres",
            host: process.env.PGHOST,
            port: parseInt(process.env.PGPORT as string),
            username: process.env.PGUSER,
            password: process.env.PGPASSWORD,
            database: process.env.PGDATABASE,
            entities: ['src/entities/*.ts'],
            "synchronize": false,
        });
        this.container = new Container();

        // set up bindings
        this.container.bind<B2CServer>(types.B2CServer).to(B2CServer);
        this.container.bind<Logger>(types.Logger).to(Logger).inSingletonScope();
        this.container.bind<PG>(types.PG).to(PG);
        this.container.bind<ErrorsHandler>(types.ErrorsHandler).to(ErrorsHandler);
        this.container.bind<ORMManager>(types.ORMManager).to(ORMManager).inSingletonScope();
        this.container.bind<Redis>(types.Redis).to(Redis).inSingletonScope();
        this.container.bind<BasicService>(types.BasicService).to(BasicService).inSingletonScope();

        return this.container;
    }
}