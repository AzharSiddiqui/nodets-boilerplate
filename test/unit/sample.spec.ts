

describe('proof of concept', () => {
    it('2+2 should be 4', () => {
        expect(2+2).toEqual(4);
    });
    it('true can never be false', () => {
        expect(true).toBeTruthy();
    });
});

