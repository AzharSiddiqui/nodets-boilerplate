import { Ibase } from "./base.interface";


export interface IHealth extends Ibase {
    data: {
        health: string;
        color: string;
    }
}