const types = {
    B2CServer: 'B2CServer', 
    ErrorsHandler: 'ErrorsHandler', 
    Logger: Symbol.for('Logger'),
    PG: Symbol.for('PG'), 
    ORMManager: Symbol.for('ORMManager'),
    Redis: Symbol.for('Redis'),
    BasicService: Symbol.for('BasicService'),
    AuthenticationMiddleware: Symbol.for('AuthenticationMiddleware')
    // Mongo: 'Mongo', 
    // ApiEndpoints: 'ApiEndpoints', 
    // CreditCollection: 'CreditCollection' 
  };
  
  export { types };
  