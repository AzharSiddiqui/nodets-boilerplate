import { injectable } from "inversify";

@injectable()
export class AuthService {

    constructor() { }

    public async getUser(token: string|string []| undefined) {
        // actual api call to authentication microservice should be made here with token...

        // this is dummy user data
        return {
            system_user_id: 1,
            user_id: 63763,
            client_id: 5,
            email: 'alpha@test.com',
            referral_code: 't1Blta',
            remitter_id: 63484,
            account_type: 'INDIVIDUAL',
            country_code: 'IN',
            customer_id: 'CIN53646354',
            timezone: 'Asia/Kolkata',
            account_type_id: 46281,
            isPhoneVerified: true,
            isDeviceOTPRequired: false,
        }
    }
}