import { injectable } from 'inversify';
import { IHealth } from "../models/health.interface";

@injectable()
export class BasicService {

    public health(): IHealth {
        return {
            success: true,
            data: {
                health: 'OK',
                color: 'Green'
            }
        }
    }
}