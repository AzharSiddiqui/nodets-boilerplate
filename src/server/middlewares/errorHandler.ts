import { injectable, inject } from 'inversify';
import { types } from '../../types';
import { Logger } from '../../config/logger';
import * as express from 'express';
import { boomify } from 'boom';

@injectable()
export class ErrorsHandler {
  @inject(types.Logger) private logger!: Logger;
  private postgreSQLErrorMapper: IpostgreSQLErrorMapper = {
    22023: 400,
    P0002: 500,
  }
  constructor() { }

  public async handler(err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
      const errorCode: (keyof IpostgreSQLErrorMapper) = err.code;
      const _err = err.isBoom ? err : boomify(err, { statusCode: err.code ? this.postgreSQLErrorMapper[errorCode] : 500 });

      const payload = {
        error: _err.output.payload.error,
        message: _err.message,
        statusCode: _err.output.payload.statusCode,
      };
      if (process.env.NODE_ENV === 'development') this.logger.error(`Stack: ${_err.stack}`);
      this.logger.error(`Name: ${payload.error} | message: ${payload.message} | status: ${payload.statusCode}`);
      res.status(payload.statusCode).json({
        success: false,
        data: payload,
      });
      next();
    }
    catch (e) {
      this.logger.error(`${e.message}`);
    }
  }
}

interface IpostgreSQLErrorMapper {
  22023: number,
  P0002: number
}