import { BaseMiddleware } from "inversify-express-utils";
import { Logger } from "../../config/logger";
import { inject } from "inversify";
import { types } from "../../types";
import { unauthorized } from 'boom';
import * as express from 'express';


export class AuthenticationMiddleware extends BaseMiddleware {

    @inject(types.Logger) private logger!: Logger;
    public handler(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        this.httpContext.user.isAuthenticated()
        .then(function(value) {
            if(!value) {
                throw unauthorized('Authentication failed');
            }
            next();
        })
        .catch(function (err) {
            next(err);
        });
    }

}