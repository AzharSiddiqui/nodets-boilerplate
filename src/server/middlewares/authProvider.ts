import { injectable, inject } from "inversify";
import { interfaces } from "inversify-express-utils";
import { Principal } from './principal';
import { AuthService } from "../../services/auth";
import * as express from 'express';

const authService = inject("AuthService");

@injectable()
export class AuthProvider implements interfaces.AuthProvider {

    @authService private readonly _authService: AuthService;

    public async getUser(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ): Promise<interfaces.Principal> {
        const token = req.headers["x-access-token"]
        const user = await this._authService.getUser(token);
        const principal = new Principal(user);
        return principal;
    }

}