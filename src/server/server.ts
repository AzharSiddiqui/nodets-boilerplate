import 'reflect-metadata';
import { injectable, inject } from 'inversify';
import { InversifyExpressServer } from 'inversify-express-utils';
import { Server } from 'http';
import { Logger } from '../config/logger';
import { types } from '../types';
import { container } from '../container.config';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import { ErrorsHandler } from './middlewares/errorHandler';
import { load } from 'inversify-express-doc';
import { createConnection } from 'typeorm';
import morgan from 'morgan';
import { AuthProvider } from './middlewares/authProvider';



@injectable()
export class B2CServer {

    @inject(types.Logger) private logger!: Logger;
    @inject(types.ErrorsHandler) private errorsHandler!: ErrorsHandler;

    constructor() { }

    public async start(): Promise<void> {
        try {
            this.logger.info(`The current environment is ${process.env.NODE_ENV}`);
            const server = new InversifyExpressServer(container, null, { rootPath: '/api' }, null, AuthProvider);

            await this.initPostgresConnection();

            server.setConfig((app: express.Application) => {
                app.use(bodyParser.urlencoded({ extended: true }));
                app.use(bodyParser.json());
                app.use(morgan(':method :status :res[content-length] - :response-time ms', {
                    stream: {
                        write: (message: string) => this.logger.info(message)
                    }
                }));
                app.disable('x-powered-by');
                app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
                    next();
                });
            });

            server.setErrorConfig((app) => {
                app.use(this.errorsHandler.handler.bind(this.errorsHandler));
            });

            const app = server.build();
            const appServer = app.listen(process.env.PORT);
            this.registerGracefulShutdownHandler(appServer);

            this.logger.info(`Express Server Up and Running @PORT: ${process.env.PORT} | at localhost`);
            load(container);
        }
        catch (e) {
            // log your message here
            this.logger.error(`Error starting server ${e.message} ${e.stack}`);
        }
    }

  
    private async initPostgresConnection() {
        //Initialize typeORM
        const entitiesPath = ['LOCAL'].includes(process.env.NODE_ENV as string) ? 'src/entities/*.ts' : 'dist/src/entities/*.js'
        await createConnection({
            type: "postgres",
            host: process.env.PGHOST,
            port: parseInt(process.env.PGPORT as string),
            username: process.env.PGUSER,
            password: process.env.PGPASSWORD,
            database: process.env.PGDATABASE,
            entities: [entitiesPath],
            "synchronize": false,
        });
    }
    private registerGracefulShutdownHandler(appServer: Server): void {
        const gracefulShutdown = (err: any): void => {
            const timestamp = Date.now();
            if (err) {
                this.logger.info(`ProcessId: ${timestamp} uncaughtException: ${err.message}`);
                this.logger.info(`ProcessId: ${timestamp} Shutting down gracefully.`);

                this.logger.error(`ProcessId: ${timestamp} uncaughtException: ${err.message}`);
                this.logger.error(`ProcessId: ${timestamp}`, err.stack);
            } else {
                this.logger.info(`ProcessId: ${timestamp} Received kill signal, shutting down gracefully.`);
            }
            /** Quit redis connection  */
            // client.quit();
            // stop reciving connections.
            appServer.close(() => {
                this.logger.info(`Closed out remaining connections. ProcessId: ${timestamp}`);
                if (err) {
                    this.logger.info('Shutting Down Forcefully');
                    setTimeout(() => {
                        process.exit(1);
                    }, 6 * 1000);
                } else {
                    this.logger.info('Shutting Down');
                    process.exit(0);
                }
            });

            // Wait for 20 second to close all open connections and processes before hard shutdown
            setTimeout(() => {
                this.logger.error(`Could not close connections in time, forcefully shutting down. ProcessId: ${timestamp}`);
                this.logger.info('Shutting Down Forcefully');
                process.exit(1);
            }, 20 * 1000);
        }

        // listen for TERM signal .e.g. kill
        process.on('SIGTERM', gracefulShutdown);

        // listen for INT signal e.g. Ctrl-C
        process.on('SIGINT', gracefulShutdown);

        // uncaughtException Exception
        process.on('uncaughtException', (err) => gracefulShutdown(err));
    }
}