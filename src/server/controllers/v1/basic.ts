import { controller, httpGet, BaseHttpController } from 'inversify-express-utils';
import * as express from 'express';
import { inject } from 'inversify';
import { PG } from '../../../db/pg';
import { types } from '../../../types';
import { partner } from '../../../entities/partner';
import { ORMManager } from '../../../db/db';
import { Redis } from '../../../db/redis';
import { BasicService } from '../../../services/basic';




@controller('/v1')
export class BasicController {

    @inject(types.PG) private pg!: PG;
    @inject(types.ORMManager) private conn!: ORMManager;
    @inject(types.Redis) private redis: Redis;
    @inject(types.BasicService) private basicService: BasicService;


    @httpGet("/")
    public default(req: express.Request, res: express.Response) {
        return res.json({
            success: 'OK'
        });
    }

    @httpGet("/health")
    public healthCheck() {
        try {
            return this.basicService.health();
        }
        catch (e) {
            throw e;
        }
    }

    @httpGet("/checkDB")
    private async checkDB(req: express.Request, res: express.Response) {
        try {
            const { rows } = await this.pg.query(`SELECT email, user_id from user_account where status='APPROVED' limit 2;`, []);
            return res.json({
                success: true,
                data: rows
            });
        }
        catch (e) {
            throw e;
        }
    }

    @httpGet("/checkORM", types.AuthenticationMiddleware)
    public async checkORM() {
        try {
            const result = await this.conn.manager
                .createQueryBuilder(partner, "partner")
                .getMany()
            return {
                success: true,
                data: {
                    message: 'ORM connection established successfully.',
                    result: result
                },
            };
        } catch(e) {
        throw e;
    }
}

@httpGet('/checkRedis')
private async checkRedis() {
    try {
        await this.redis.setValue('Hello', 'World')
        const result = await this.redis.getValue('Hello');
        return {
            success: true,
            data: {
                message: 'Redis running successfully.',
                result: result
            },
        };
    }
    catch (e) {
        throw e;
    }
}
}
