import 'reflect-metadata';
import { injectable, inject } from 'inversify';
import { Pool, PoolConfig } from 'pg';
import { Logger } from '../config/logger';
import { types } from '../types';

@injectable()
export class PG {

    @inject(types.Logger) private logger!: Logger;
    public pool: Pool;
    constructor() {
        this.pool = new Pool(this.getConnectionConfig());

        this.pool.on('error', (err) => {
            this.logger.error(`Postgres connection error on client - ${err.message}`);
            throw err;
        });
    }

    private getConnectionConfig(): PoolConfig {
        return {
            user: process.env.PGUSER,
            host: process.env.PGHOST,
            database: process.env.PGDATABASE,
            password: process.env.PGPASSWORD,
            port: Number(process.env.PGPORT),
            max: Number(process.env.PGMAX),
            idleTimeoutMillis: Number(process.env.PGIDLETIMEOUT),
            connectionTimeoutMillis: Number(process.env.CONNECTIONTIMEOUT),
        }
    }

    public async getClient() {
        return await this.pool.connect();
    }

    public query(text: string, params: Array<any>) {
        return this.pool.query(text, params);
    }
}