import { injectable, inject } from "inversify";
import { Tedis } from 'redis-typescript';
import { types } from '../types';
import { Logger } from '../config/logger';

@injectable()
export class Redis {
    
    public client!: Tedis;
    @inject(types.Logger) private logger!: Logger;

    constructor() {
        this.client = new Tedis({
            host: process.env.REDIS_HOST,
            port: parseInt(process.env.REDIS_PORT as string)
        });
    }


    public async setValue(key:string, value:string) {
        try {
            await this.client.set(key, value);
        } catch (e) {
            throw e;
        }
    }

    public async getValue(key:string) : Promise<string | number | null> {
        try {
            return await this.client.get(key);
            
        } catch(e) {
            throw e;
        }
    }
}