import { injectable } from "inversify";
import { EntityManager, getManager } from "typeorm";


@injectable()
export class ORMManager {
    public manager: EntityManager;
    constructor() {
        this.manager = getManager();
    }
}