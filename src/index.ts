import { B2CServer } from './server/server';
import { container } from './container.config';
import { types } from './types';
import * as dotenv from 'dotenv';

dotenv.config();
container.get<B2CServer>(types.B2CServer).start();