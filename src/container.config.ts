import { Container } from 'inversify';
import { types } from './types';
import { B2CServer } from './server/server';
import { Logger } from './config/logger';
import { ErrorsHandler } from './server/middlewares/errorHandler';
import { PG } from './db/pg';
import { ORMManager } from './db/db';


// declare metadata @controller
import './server/controllers/v1/basic';
import { Redis } from './db/redis';
import { BasicService } from './services/basic';
import { AuthService } from './services/auth';
import { AuthenticationMiddleware } from './server/middlewares/authentication.middleware';



// set up container
const container = new Container();

// set up bindings
container.bind<B2CServer>(types.B2CServer).to(B2CServer);
container.bind<Logger>(types.Logger).to(Logger).inSingletonScope();
container.bind<PG>(types.PG).to(PG);
container.bind<ErrorsHandler>(types.ErrorsHandler).to(ErrorsHandler);
container.bind<ORMManager>(types.ORMManager).to(ORMManager).inSingletonScope();
container.bind<Redis>(types.Redis).to(Redis).inSingletonScope();
container.bind<BasicService>(types.BasicService).to(BasicService).inSingletonScope();
container.bind<AuthenticationMiddleware>(types.AuthenticationMiddleware).to(AuthenticationMiddleware);
container.bind('AuthService').to(AuthService);



// export container
export { container };
