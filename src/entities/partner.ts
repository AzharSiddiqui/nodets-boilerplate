import {Column,Entity,Index,PrimaryGeneratedColumn} from 'typeorm';


@Entity("partner",{schema:"public" } )
@Index("partner_partner_key_key",["partner_key",],{unique:true})
export class partner {

    @PrimaryGeneratedColumn({
        type:"smallint", 
        name:"partner_id"
        })
    partner_id:number;
        

    @Column("character varying",{ 
        nullable:true,
        length:100,
        name:"name"
        })
    name:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:100,
        name:"description"
        })
    description:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        unique: true,
        length:100,
        name:"partner_key"
        })
    partner_key:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:100,
        name:"type"
        })
    type:string | null;
        

    @Column("jsonb",{ 
        nullable:true,
        name:"config"
        })
    config:Object | null;
        

    @Column("boolean",{ 
        nullable:true,
        default: () => "true",
        name:"is_active"
        })
    is_active:boolean | null;
        

    @Column("timestamp with time zone",{ 
        nullable:true,
        default: () => "timezone('UTC', now())",
        name:"created_at"
        })
    created_at:Date | null;
        

    @Column("integer",{ 
        nullable:true,
        name:"created_by"
        })
    created_by:number | null;
        

    @Column("timestamp with time zone",{ 
        nullable:true,
        default: () => "timezone('UTC', now())",
        name:"updated_at"
        })
    updated_at:Date | null;
        

    @Column("integer",{ 
        nullable:true,
        name:"updated_by"
        })
    updated_by:number | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:100,
        name:"code"
        })
    code:string | null;
        

    @Column("jsonb",{ 
        nullable:true,
        default: () => "'{}'",
        name:"notification_config"
        })
    notification_config:Object | null;
        
}
