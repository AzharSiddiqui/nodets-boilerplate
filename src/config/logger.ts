import { injectable } from 'inversify';
import { createLogger, transports, Logger as Iwinston, LoggerOptions, format } from 'winston';


@injectable()
export class Logger {

    public logger: Iwinston;
    constructor() {
        this.logger = createLogger(this.logOptions());
        this.addTransportConsole();
    };

    private logOptions(): LoggerOptions {
        return {
            level: process.env.LOGGER_LEVEL || 'info',
            format: format.combine(
                format.timestamp(),
                format.json(),
                format.colorize()
            ),
        }
    }
    private getTransportConsoleOptions(): transports.ConsoleTransportOptions {
        return {
            debugStdout: false,
        };
    }

    private addTransportConsole() {
        this.logger.add(new transports.Console(this.getTransportConsoleOptions()));
    }

    public info(message: string, data?: any): void {
        this.logger.info(message, data);
    }

    public error(message: string, data?: { error: any }): void {
        this.logger.error(message, data);
    }

}