module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  collectCoverage: true,
  testMatch: ['**/test/**/*.spec.ts'],
  reporters: ['default','jest-stare'],
  testResultsProcessor: 'jest-stare',
  setupFilesAfterEnv: ["jest-extended"]
};