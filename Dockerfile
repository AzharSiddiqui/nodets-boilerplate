FROM node:10.13-alpine
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm i
COPY . .
RUN npm run build
EXPOSE 4400 
ENTRYPOINT node dist/src/index.js